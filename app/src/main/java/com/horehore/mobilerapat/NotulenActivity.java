package com.horehore.mobilerapat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class NotulenActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView dateTextfield;
    private TextView timeTextfield;
    private EditText placeTextfield;
    private EditText titleTextfield;
    private EditText notulisTextfield;
    private EditText hadirTextfield;
    private EditText absenTextfield;
    private EditText rosterTextfield;
    private EditText topicTextfield;
    private EditText hasilTextfield;

    public static final String KEY_DATE="date";
    public static final String KEY_TIME="time";
    public static final String KEY_PLACE="place";
    public static final String KEY_TITLE="title";
    public static final String KEY_NOTULIS="notulis";
    public static final String KEY_HADIR="hadir";
    public static final String KEY_ABSEN="absen";
    public static final String KEY_ROSTER="roster";
    public static final String KEY_TOPIC="topic";
    public static final String KEY_HASIL="hasil";
    public static final String KEY_EMAIL="email";

    private String date;
    private String time;
    private String place;
    private String title;
    private String notulis;
    private String hadir;
    private String absen;
    private String roster;
    private String topic;
    private String hasil;


    private Button notulenButton;

    private static final String NOTULEN_URL = "http://horehore.esy.es/volleyNotulen.php";
    private String valueku;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notulen);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("KEY_EMAIL");
            valueku = value;
            TextView text = (TextView) findViewById(R.id.textView);
            text.setText(valueku);
        }

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        // formattedDate have current date/time

        SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
        String formatDate = tf.format(c.getTime());



        TextView text = (TextView) findViewById(R.id.textDate);
        text.setText(formattedDate);
        TextView texi = (TextView) findViewById(R.id.textTime);
        texi.setText(formatDate);




        dateTextfield = (TextView) findViewById(R.id.textDate);
        timeTextfield = (TextView) findViewById(R.id.textTime);
        placeTextfield = (EditText) findViewById(R.id.placeText);
        titleTextfield = (EditText) findViewById(R.id.titleText);
        notulisTextfield = (EditText) findViewById(R.id.notulisText);
        hadirTextfield = (EditText) findViewById(R.id.hadirText);
        absenTextfield = (EditText) findViewById(R.id.absenText);
        rosterTextfield = (EditText) findViewById(R.id.rosterText);
        topicTextfield = (EditText) findViewById(R.id.topicText);
        hasilTextfield = (EditText) findViewById(R.id.hasilText);

        notulenButton = (Button) findViewById(R.id.notulenButton);

        notulenButton.setOnClickListener(this);
    }

    private void notul() {
        date = dateTextfield.getText().toString();
        time = timeTextfield.getText().toString();
        place = placeTextfield.getText().toString();
        title = titleTextfield.getText().toString();
        notulis = notulisTextfield.getText().toString();
        hadir = hadirTextfield.getText().toString();
        absen = absenTextfield.getText().toString();
        roster = rosterTextfield.getText().toString();
        topic = topicTextfield.getText().toString();
        hasil = hasilTextfield.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NOTULEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.trim().equals("success")){
                            Toast.makeText(NotulenActivity.this,response,Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(NotulenActivity.this,response,Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(NotulenActivity.this,error.toString(),Toast.LENGTH_LONG ).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put(KEY_DATE,date);
                params.put(KEY_TIME,time);
                params.put(KEY_PLACE,place);
                params.put(KEY_TITLE,title);
                params.put(KEY_NOTULIS,notulis);
                params.put(KEY_HADIR,hadir);
                params.put(KEY_ABSEN,absen);
                params.put(KEY_ROSTER,roster);
                params.put(KEY_TOPIC,topic);
                params.put(KEY_HASIL,hasil);
                params.put(KEY_EMAIL,valueku);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onClick(View v)
    {
        notul();
    }
}


