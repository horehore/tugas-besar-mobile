package com.horehore.mobilerapat;

    import android.os.Bundle;
    import android.support.v7.app.AppCompatActivity;
    import android.view.View;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.Toast;

    import com.android.volley.AuthFailureError;
    import com.android.volley.Request;
    import com.android.volley.RequestQueue;
    import com.android.volley.Response;
    import com.android.volley.VolleyError;
    import com.android.volley.toolbox.StringRequest;
    import com.android.volley.toolbox.Volley;

    import java.util.HashMap;
    import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText nameTextfield;
    private EditText mailTextfield;
    private EditText passTextfield;
    private EditText hpTextfield;
    private EditText idTextfield;

    private Button registerButton;

    public static final String REGISTER_URL = "http://horehore.esy.es/volleyRegister.php";
    public static final String KEY_EMAIL="email";
    public static final String KEY_PASSWORD="password";
    public static final String KEY_NAME="name";
    public static final String KEY_ID="id";
    public static final String KEY_HP="hp";


    private String email;
    private String password;
    private String name;
    private String hp;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mailTextfield = (EditText) findViewById(R.id.mailTextField);
        nameTextfield = (EditText) findViewById(R.id.nameTextfield);
        passTextfield = (EditText) findViewById(R.id.passTextField);
        hpTextfield = (EditText) findViewById(R.id.hptextField);
        idTextfield = (EditText) findViewById(R.id.idTextField);

        registerButton = (Button) findViewById(R.id.registerButton);

        registerButton.setOnClickListener(this);
    }


    private void userRegis() {
        email = mailTextfield.getText().toString();
        name = nameTextfield.getText().toString();
        password = passTextfield.getText().toString();
        hp = hpTextfield.getText().toString();
        id = idTextfield.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.trim().equals("success")){
                            Toast.makeText(SignUpActivity.this,response,Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(SignUpActivity.this,response,Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SignUpActivity.this,error.toString(),Toast.LENGTH_LONG ).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put(KEY_EMAIL,email);
                params.put(KEY_PASSWORD,password);
                params.put(KEY_NAME,name);
                params.put(KEY_ID,id);
                params.put(KEY_HP,hp);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onClick(View v)
    {
        userRegis();
    }
}